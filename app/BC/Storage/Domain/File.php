<?php

namespace App\BC\Storage\Domain;

use App\Framework\Domain\DefaultDomain;

class File extends DefaultDomain
{
    protected String $bucket;
    protected String $path;
    protected String $mime;
    protected String $filename;

    public function setBucket(String $bucket)
    {
        $this->bucket = $bucket;
    }

    public function setPath(String $path)
    {
        $this->path = $path;
    }

    public function setMime(String $mime)
    {
        $this->mime = $mime;
    }

    public function setFilename(String $filename)
    {
        $this->filename = $filename;
    }

    public function toArray(): array
    {
        return [
            'bucket' => $this->bucket,
            'path' => $this->path,
            'mime' => $this->mime,
            'filename' => $this->filename,
        ];
    }
}
