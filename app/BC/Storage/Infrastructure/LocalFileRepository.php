<?php

namespace App\BC\Storage\Infrastructure;

use App\BC\Storage\Domain\File;
use App\BC\Storage\Domain\FileRepository;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class LocalFileRepository implements FileRepository
{
    public function write(String $bucket, String $path, UploadedFile $file): File
    {
        try {
            $fullpath = $this->generateFullPath($bucket, $path);
            $file->storeAs($fullpath, $file->getClientOriginalName());

            $entry = new File();
            $entry->setBucket($bucket);
            $entry->setPath($path);
            $entry->setMime($file->getClientMimeType());
            $entry->setFilename($file->getClientOriginalName());

            return $entry;
        } catch (Exception $exception) {
            throw new Exception($exception);
        }
    }

    public function read(String $bucket, String $path): File
    {
        try {
            if (empty($bucket)) {
                throw new Exception('No bucket specified', 404);
            }

            if (empty($path)) {
                throw new Exception('No path specified', 404);
            }

            $fullpath = $this->generateFullPath($bucket, $path);
            if (Storage::exists($fullpath)) {
                $file = new File();
                $file->setBucket($bucket);
                $file->setPath($path);
                $file->setMime(Storage::mimeType($fullpath));
                $file->setFilename(basename(Storage::path($fullpath)));

                return $file;
            }

            throw new FileNotFoundException('file not found', 404);
        } catch (Exception $exception) {
            throw new Exception($exception);
        }
    }

    private function generateFullPath(String $bucket, String $path): String
    {
        return $bucket . '/' . $path;
    }
}
