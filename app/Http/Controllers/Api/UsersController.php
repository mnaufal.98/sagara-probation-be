<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RoleUsers;
use App\Services\ResponseService;

class UsersController extends Controller
{
    private $model;
    protected $reference;
    protected $forms;
    protected $response;

    public function __construct(Request $request, User $model, ResponseService $response)
    {
        try {
            if (file_exists(app_path('Models/' . Str::studly('users')) . '.php')) {
                $this->model = app("App\Models\\" . Str::studly('users'));
            } else {
                if ($model->checkTableExists('users')) {
                    $this->model = $model;
                    $this->model->setTable('users');
                }
            }

            $this->reference = $this->model->getReference();
            $this->forms = $this->model->getForms();
            $this->response = $response;
        } catch (Exception $e) {
            //throw $th;
        }
    }

    public function create(Request $request)
    {
        try {
            $validators = $this->model->validator($request);
            if ($validators->fails()) {
                return $this->validatorErrors($validators);
            }
            $fields = $request->only($this->model->getFields());
            foreach ($fields as $key => $value) {
                $this->model->setAttribute($key, $value);
            }
            $this->model['password'] = bcrypt($this->model['password']);
            $this->model->save();

            $roleId = $request['role'][0];
            $userId = $this->model->id;

            $data = [
                'users_id' => $userId,
                'roles_id' => $roleId,
                'user' => $this->model
            ];

            RoleUsers::create($data);

            return $this->response->successResponse($data, 'Data created.');
        } catch (Exception $error) {
            return $this->generalError($error);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $entry = User::where('status', 1)->where('id', $id)->get();

            if (!$entry) {
                return $this->response->notFoundResponse();
            }

            $validators = $this->model->validator($request);
            if ($validators->fails()) {
                return $this->validatorErrors($validators);
            }
            $fields = $request->only($this->model->getFields());
            $entry = User::where('status', 1)->where('id', $id);
            $entry->update($fields);
            $existingUsersRoles = RoleUsers::where('users_id', $id)->get();

            if ($existingUsersRoles) {
                $existingUsersRoles = RoleUsers::where('users_id', $id)->delete();
            }
            $roleId = $request['role'][0];

            $data = [
                'users_id' => $id,
                'roles_id' => $roleId,
                'user' => $entry
            ];
            RoleUsers::create($data);
            return $this->response->successResponse($data, 'Data created.');
        } catch (Exception $error) {
            return $this->generalError($error);
        }
    }

    public function read($id)
    {
        $users = User::find($id);
        if (!$users) {
            return response()->json([
                'errors' => [
                    'message' => 'Not Found',
                    'status' => 404
                ]
            ], 404);
        }

        $role = RoleUsers::where('users_id', $id)->pluck('id');
        $roleStr = strval($role[0]);
        $users['role'] = [$roleStr];

        return response()->json($users, 200);
    }
}
