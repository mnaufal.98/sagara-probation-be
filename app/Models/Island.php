<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Island extends Resources
{
  use HasFactory;

  protected $table = 'islands';
  protected $fillable = ['name'];


  public function provinces(): HasMany
  {
    return $this->hasMany(Province::class, 'id_island');
  }
}